from sqlalchemy import Column, DateTime, Integer, String
from sqlalchemy.sql import func
from db_conf import Base


class Post(Base):
    __tablename__ = "post"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(255))
    author = Column(String(255))
    content =Column(String(255))
    time_created = Column(DateTime(timezone=True), server_default=func.now())