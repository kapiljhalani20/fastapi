# to build docker
docker-compose build
# for migration
docker-compose run app alembic revision --autogenerate -m "New Migration"
# to run alembic 
docker-compose run app alembic upgrade head
